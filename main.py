import os
import tkinter
import settings.binder as binder
import time

from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox

import converter

def PopulateManagerFrame(function_settings : dict):  
    function_settings["manager_frame"].grid(column=0, row=0, sticky="nesw")
    function_settings["manager_frame"]['padx'] = 5
    function_settings["manager_frame"]['pady'] = 5
    function_settings["manager_frame"].rowconfigure(0, weight=1)
    function_settings["manager_frame"].rowconfigure(1, weight=1)
    function_settings["manager_frame"].columnconfigure(0, weight=1)
    
    # Handle Frame Layout for the file selection part
    file_selection_frame = tkinter.LabelFrame(function_settings["manager_frame"], text="File Selection")
    file_selection_frame.grid(column=0, row=0, sticky="nesw")
    file_selection_frame.rowconfigure(0, weight=1)
    file_selection_frame.rowconfigure(1, weight=1)
    file_selection_frame.rowconfigure(2, weight=1)
    file_selection_frame.columnconfigure(0, weight=1)
    file_selection_frame.columnconfigure(1, weight=7)
    file_selection_frame.columnconfigure(2, weight=2)

    # Construct the labels
    collection_file_name_label = tkinter.Label(file_selection_frame, text="Collection File:", anchor="w")
    collection_file_name_label.grid(row=0, column=0, sticky="ew")
    
    binder_file_name_label = tkinter.Label(file_selection_frame, text="Binder File:", anchor="w")
    binder_file_name_label.grid(row=1, column=0, sticky="ew")
    
    binder_file_name_label = tkinter.Label(file_selection_frame, text="Result File Name:", anchor="w")
    binder_file_name_label.grid(row=2, column=0, sticky="ew")
    
    # Construct the text entries
    collection_file_name = tkinter.Entry(file_selection_frame, textvariable=function_settings["collection_file_name_string"], borderwidth=2, relief=tkinter.SUNKEN)
    collection_file_name.configure(state="disabled")
    collection_file_name.grid(row=0, column=1, padx=5, sticky="ew")
    
    binder_file_name = tkinter.Entry(file_selection_frame, textvariable=function_settings["binder_file_name_string"], borderwidth=2, relief=tkinter.SUNKEN)
    binder_file_name.configure(state="disabled")
    binder_file_name.grid(row=1, column=1, padx=5, sticky="ew")
    
    result_file_name = tkinter.Entry(file_selection_frame, textvariable=function_settings["result_file_name_string"], borderwidth=2, relief=tkinter.SUNKEN)
    result_file_name.grid(row=2, column=1, padx=5, sticky="ew")
    
    # Construct the buttons and their functions
    def SelectCollectionCSV():
        file_types = [("csv files", "*.csv")]
        filename = filedialog.askopenfilename(title="Select a CSV", initialdir=os.getcwd(), filetypes=file_types)
        if len(filename) != 0:
            function_settings["collection_file_name_string"].set(filename)
            collection_file_name.xview_moveto(1)
    
    def SelectBinderCSV():
        file_types = [("csv files", "*.csv")]
        filename = filedialog.askopenfilename(title="Select a CSV", initialdir=os.getcwd(), filetypes=file_types)
        if len(filename) != 0:
            function_settings["binder_file_name_string"].set(filename)
            binder_file_name.xview_moveto(1)
            
    set_collection_file_button = tkinter.Button(file_selection_frame, text="Search", command=SelectCollectionCSV)
    set_collection_file_button.grid(row=0, column=2, padx=5, sticky="ew")

    set_binder_file_button = tkinter.Button(file_selection_frame, text="Search", command=SelectBinderCSV)
    set_binder_file_button.grid(row=1, column=2, padx=5, sticky="ew")
    
    general_settings_frame = tkinter.Frame(function_settings["manager_frame"])
    general_settings_frame.grid(row=1, column=0, sticky="nesw")
    general_settings_frame.rowconfigure(0, weight=1)
    general_settings_frame.columnconfigure(0, weight=1)  
    general_settings_frame.columnconfigure(1, weight=1)  
    general_settings_frame.columnconfigure(2, weight=1)
    
    global game_settings_frame
    game_settings_frame = tkinter.LabelFrame(general_settings_frame, text="Game")
    game_settings_frame.grid(row=0, column=0, sticky="nesw")
    for i, game in enumerate(converter.game_list):
        game_settings_frame.rowconfigure(i, weight=1)
        radio_button = tkinter.Radiobutton(game_settings_frame, text=game, variable=function_settings["game_variable"], value=i)
        radio_button.grid(row=i, column=0, sticky="w")
    
    global collection_settings_frame
    collection_settings_frame = tkinter.LabelFrame(general_settings_frame, text="Collection")
    collection_settings_frame.grid(row=0, column=1, sticky="nesw")
    
    for i, collection in enumerate(converter.collection_list):
        collection_settings_frame.rowconfigure(i, weight=1)
        radio_button = tkinter.Radiobutton(collection_settings_frame, text=collection, variable=function_settings["collection_variable"], value=i)
        radio_button.grid(row=i, column=0, sticky="w")
        
    function_buttons_frame = tkinter.Frame(general_settings_frame)
    function_buttons_frame.grid(row=0, column=2)
    function_buttons_frame.columnconfigure(0, weight=1)
    function_buttons_frame.rowconfigure(0, weight=1)
    function_buttons_frame.rowconfigure(1, weight=1)
    function_buttons_frame.rowconfigure(2, weight=1)
    
    global load_collection_button
    load_collection_button = tkinter.Button(function_buttons_frame, text="Load Collection", command=function_settings["load_collection_function"])
    load_collection_button.grid(row=0, column=0, pady=2, sticky="ew")
    
    global convert_button
    convert_button = tkinter.Button(function_buttons_frame, text="Convert", command=function_settings["convert_function"])
    convert_button.configure(state="disabled")
    convert_button.grid(row=1, column=0, pady=2, sticky="ew")
    
    global export_button
    export_button = tkinter.Button(function_buttons_frame, text="Export", command=function_settings["export_function"])
    export_button.configure(state="disabled")
    export_button.grid(row=2, column=0, pady=2, sticky="ew")

def PopulatematchesFrame(function_settings : dict):
    # Set base frame layout
    function_settings["matches_frame"].grid(column=1, row=0, stick="nesw")
    function_settings["matches_frame"]['padx'] = 5
    function_settings["matches_frame"]['pady'] = 5
    function_settings["matches_frame"].rowconfigure(0, weight=1)
    function_settings["matches_frame"].rowconfigure(1)
    function_settings["matches_frame"].rowconfigure(2, weight=1)
    function_settings["matches_frame"].rowconfigure(3)
    function_settings["matches_frame"].columnconfigure(0, weight=1)
    
    global collection_list_box 
    collection_list_box = tkinter.Listbox(function_settings["matches_frame"], listvariable=function_settings["collection_list_box_variable"], exportselection=False, selectmode=tkinter.SINGLE, activestyle=tkinter.NONE)
    collection_list_box.grid(row=0, column=0, sticky="nesw")
    collection_list_box.bind("<<ListboxSelect>>", function_settings["collection_list_box_selection_callback"])
    collection_list_box.bind('<FocusOut>', lambda e: collection_list_box.selection_clear(0, tkinter.END))
    
    global partial_matches_label
    partial_matches_label = tkinter.Label(function_settings["matches_frame"], text="Partial matches: 0")
    partial_matches_label.grid(row=1, column=0, pady=2, stick="ns")

    global matches_list_box
    matches_list_box = tkinter.Listbox(function_settings["matches_frame"], listvariable=function_settings["match_list_box_variable"], exportselection=False, selectmode=tkinter.SINGLE, activestyle=tkinter.NONE)
    matches_list_box.configure(state="disabled")
    matches_list_box.grid(row=2, column=0, pady=5, sticky="nesw")
    matches_list_box.bind("<<ListboxSelect>>", function_settings["match_list_box_selection_callback"])
    matches_list_box.bind('<FocusOut>', lambda e: matches_list_box.selection_clear(0, tkinter.END))
    
    match_button_frame = tkinter.Frame(function_settings["matches_frame"])
    match_button_frame.grid(row=3, column=0, sticky="nesw")
    match_button_frame.rowconfigure(0, weight=1)
    match_button_frame.columnconfigure(0, weight=1)
    match_button_frame.columnconfigure(1, weight=1)
    
    global select_match_button
    select_match_button = tkinter.Button(match_button_frame, text="Select Match", command=function_settings["select_match_function"])
    select_match_button.configure(state="disabled")
    select_match_button.grid(row=0, column=0, sticky="ns")
    
    global bad_match_button
    bad_match_button = tkinter.Button(match_button_frame, text="Bad Match", command=function_settings["bad_match_function"])
    bad_match_button.configure(state="disabled")
    bad_match_button.grid(row=0, column=1, sticky="ns")
    
def ConvertToCollectionListDisplay(collection_item : dict, game_variable : tkinter.IntVar) -> str:
    return converter.GetReadableLine(0, game_variable.get(), collection_item)

def CreateMatchDisplay(container):
    frame = ttk.Frame(container)
    
    return frame

def CreateMainWindow():
    main_window = tkinter.Tk()
    main_window.title("Scan to Binder")
    main_window.geometry("1000x400")
    main_window['padx'] = 10
    main_window['pady'] = 10
    main_window.rowconfigure(0, weight=1)
    main_window.columnconfigure(0, weight=2)
    main_window.columnconfigure(1, weight=8)
    
    # Create whats needed for the managing side of the program (left side)
    
    # Sets up the stuff for doing the converting
    # Everything will work based on the ordering of this list
    collection = list()
    
    # Set up layout and needed variables
    manager_frame = tkinter.Frame(main_window)
    collection_file_name_string = tkinter.StringVar()
    binder_file_name_string = tkinter.StringVar()
    result_file_name_string = tkinter.StringVar()
    game_variable = tkinter.IntVar()
    collection_variable = tkinter.IntVar()
    collection_list_box_variable = tkinter.Variable()
    match_list_box_variable = tkinter.Variable()
    global collection_selection_index
    collection_selection_index = 0
    global match_selection_index
    match_selection_index = 0
    global number_partial_matches
    number_partial_matches = 0
    
    def ChangeListBoxColour():
        for collection_item in collection:
            if "bad_match" in collection_item:
                collection_list_box.itemconfig(collection_item["id"], bg="grey")
                continue
            if "perfect_match" in collection_item:
                if collection_item["perfect_match"]:
                    collection_list_box.itemconfig(collection_item["id"], bg="green")
                else:
                    collection_list_box.itemconfig(collection_item["id"], bg="red")
                    
    def CollectionListBoxSelectCallback(event):
        selection = event.widget.curselection()
        converted_matches = list()
        select_match_button.configure(state="disabled")
        if selection:
            if "perfect_match" in collection[selection[0]]:
                if "bad_match" not in collection[selection[0]]:
                    bad_match_button.configure(state="normal")
                else:
                    bad_match_button.configure(state="disabled")                    
                for binder_line in collection[selection[0]]["matches"]:
                    converted_matches.append(binder.GetReadableLine(binder_line))
                match_list_box_variable.set(converted_matches)
                if collection[selection[0]]["perfect_match"] or "bad_match" in collection[selection[0]]:
                    matches_list_box.configure(state="disabled")
                else:
                    matches_list_box.configure(state="normal")
            global collection_selection_index
            collection_selection_index = selection[0]
                    
    def MatchListBoxSelectCallback(event):
        selection = event.widget.curselection()
        if selection:
            global match_selection_index
            match_selection_index = selection[0]
            select_match_button.configure(state="normal")
            bad_match_button.configure(state="disabled")                    
            
    # The function for loading the collection file into the system
    def LoadCollectionFunction():
        if len(collection_file_name_string.get()) != 0:
            # Loads the given collection into the system as manageable data
            if converter.LoadCollectionInput(collection_file_name_string.get(), collection, game_variable.get(), collection_variable.get()):
                converted_collection = list()
                for collection_item in collection:
                    converted_collection.append(ConvertToCollectionListDisplay(collection_item, game_variable))
                collection_list_box_variable.set(converted_collection)
                load_collection_button.configure(state="disabled")
                convert_button.configure(state="normal")
                for child in game_settings_frame.winfo_children():
                    child.configure(state="disabled")
                for child in collection_settings_frame.winfo_children():
                    child.configure(state="disabled")
            return
        messagebox.showerror("Import Error", "No Collection File Supplied")
        
    def UpdateCollectionText():
        new_collection_list = list()
        partial_match_count = 0
        for collection_item in collection:
            if "perfect_match" in collection_item:
                if collection_item["perfect_match"]:
                    new_collection_list.append(binder.GetReadableLine(collection_item["matches"][0]))
                else:
                    if "bad_match" not in collection_item:
                        partial_match_count += 1
                    new_collection_list.append(ConvertToCollectionListDisplay(collection_item, game_variable))
        if partial_match_count == 0:
            collection_list_box.config(bg="green")
            partial_matches_label.config(text="Partial matches: 0", fg="green")
        else:
            collection_list_box.config(bg="red")
            partial_matches_label.config(text="Partial matches: " + str(partial_match_count), fg="red")
        collection_list_box_variable.set(new_collection_list)
        ChangeListBoxColour()
            
    def ConvertFunction():
        if len(binder_file_name_string.get()) !=0:
            if len(collection) == 0:
                messagebox.showerror("Convert Error", "No collection has been loaded")
                return
            if converter.GenerateBinderInput(binder_file_name_string.get(), collection, game_variable.get(), collection_variable.get()):
                UpdateCollectionText()
                convert_button.configure(state="disabled")
                export_button.configure(state="normal")
            return
        messagebox.showerror("Convert Error", "No Binder File Supplied")
        
    def SelectMatchFunction():
        global collection_selection_index
        global match_selection_index
        collection[collection_selection_index]["perfect_match"] = True
        new_match_list = list([collection[collection_selection_index]["matches"][match_selection_index]])
        collection[collection_selection_index]["matches"] = new_match_list
        UpdateCollectionText()
        collection_list_box.selection_clear(0, tkinter.END)
        matches_list_box.selection_clear(0, tkinter.END)
        select_match_button.configure(state="disabled")
        match_list_box_variable.set([])
    
    def BadMatchFunction():
        collection[collection_selection_index]["bad_match"] = True
        UpdateCollectionText()
        collection_list_box.selection_clear(0, tkinter.END)
        matches_list_box.selection_clear(0, tkinter.END)
        select_match_button.configure(state="disabled")
        match_list_box_variable.set([])
        bad_match_button.configure(state="disabled")
        
    def ExportFunction():
        file_name = result_file_name_string.get()
        for collection_item in collection:
            if not collection_item["perfect_match"] and "bad_match" not in collection_item:
                messagebox.showerror("Partial Matches", "There are some partial matches left.")
                return
        if len(file_name) !=0:
            if len(file_name.split()) != 1:
                file_name = file_name.split()[0]
            file_name += ".csv"
            converter.ExportFile(file_name, collection)
            return
        messagebox.showerror("Export Error", "No File Name Supplied") 

    manager_function_settings = {"manager_frame": manager_frame, 
                                 "collection_file_name_string": collection_file_name_string,
                                 "binder_file_name_string": binder_file_name_string,
                                 "result_file_name_string": result_file_name_string,
                                 "game_variable": game_variable,
                                 "collection_variable": collection_variable,
                                 "load_collection_function": LoadCollectionFunction,
                                 "convert_function": ConvertFunction,
                                 "export_function" : ExportFunction}
    PopulateManagerFrame(manager_function_settings)
    
    # Create what's needed for the matches side of the program (right side)
    matches_frame = tkinter.LabelFrame(main_window, text="Matches")  
    
    matches_function_settings = {"matches_frame": matches_frame,
                                 "collection_list_box_variable": collection_list_box_variable,
                                 "match_list_box_variable": match_list_box_variable,
                                 "collection_list_box_selection_callback": CollectionListBoxSelectCallback,
                                 "match_list_box_selection_callback": MatchListBoxSelectCallback,
                                 "select_match_function" : SelectMatchFunction,
                                 "bad_match_function" : BadMatchFunction}
    PopulatematchesFrame(matches_function_settings)
    
    main_window.mainloop()
    
if __name__ == "__main__":
    CreateMainWindow()