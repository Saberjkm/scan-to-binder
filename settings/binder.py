import_field_names = ["Game Type", 
                      "Binder Id", 
                      "Variant Title", 
                      "Card Name", 
                      "Set Name", 
                      "In stock", 
                      "Current Price",
                      "SKU",
                      "Handle",
                      "Product Title",
                      "Shopify Variant Id",
                      "Barcode",
                      "Variant Id",
                      "Add Stock",
                      "Set Stock",
                      "Reserve Quantity",
                      "Price Override",
                      "Cash Buy Price",
                      "Cash Buy Percent",
                      "Store Credit Buy Price",
                      "Store Credit Buy Percent",
                      "Buy Limit", 
                      "Overstocked Cash Buy Price",
                      "Overstocked Cash Buy Percentage",
                      "Overstocked Store Credit Buy Price",
                      "Overstocked Store Credit Buy Percentage"]

game_codes = ["mtg", "yugioh", "pokemon", "fleshAndBlood"]

quantity_change_column_name = "Add Stock"
# Returns a binder line in readable form
def GetReadableLine(binder_line : dict) -> str:
    return_string = str()
    if "Product Title" in binder_line:
        return_string += binder_line["Product Title"]
    return_string += " "
    if "SKU" in binder_line:
        return_string += "["
        return_string += binder_line["SKU"]
        return_string += "]"
    return return_string