from tkinter import messagebox
import settings.dragonshield.mtg as mtg
import settings.dragonshield.yugioh as yugioh
import settings.dragonshield.pokemon as pokemon
import settings.dragonshield.fleshandblood as fleshandblood

#mtg, yugioh, pokemon
encoding = ["utf8", "utf-16-le", "utf-16-le", "utf-16-le"]

mtg_processed_item_generators = list([mtg.BuildSKU])

mtg_settings = {
    "generators": mtg_processed_item_generators,
    "validator": mtg.ValidateCollection
}

yugioh_processed_item_generators = list([yugioh.BuildSKU])

yugioh_settings = {
    "generators" : yugioh_processed_item_generators,
    "validator": yugioh.ValidateCollection
}

pokemon_processed_item_generators = list([pokemon.BuildSKU])

pokemon_settings = {
    "generators" : pokemon_processed_item_generators,
    "validator" : pokemon.ValidateCollection
}

fleshandblood_processed_item_generators = list([fleshandblood.BuildSKU])

fleshandblood_settings = {
    "generators" : fleshandblood_processed_item_generators,
    "validator" : fleshandblood.ValidateCollection
}

def CullMatches(collection : list, game_id : int):
    match game_id:
        case 0:
            mtg.CullMatches(collection)
        case 1: 
            yugioh.CullMatches(collection)
        case 2:
            pokemon.CullMatches(collection)
        case 3:
            fleshandblood.CullMatches(collection)

# After culling evaluates remaining matches to see if 1 remains, if so set it to be a perfect match
def EvaluatePerfectMatches(collection : list):
    for collection_item in collection:
        if len(collection_item["matches"]) == 1:
            collection_item["perfect_match"] = True
            
def GetEncoding(game_id : int) -> str:
    return encoding[game_id]

def NormaliseSKU(sku : str, game_id : int) -> list:
    match game_id:
        case 0:
            #mtg
            return mtg.NormaliseSKU(sku)
        case 1:
            #yugioh
            return yugioh.NormaliseSKU(sku)
        case 2:
            #pokemon
            return pokemon.NormaliseSKU(sku)
        case 3:
            #flesh and blood
            return fleshandblood.NormaliseSKU(sku)

def GetCollectionInputSettings(game_id : int) -> dict:
    match game_id:
        case 0:
            #mtg
            return mtg_settings
        case 1:
            #yugioh
            return yugioh_settings
        case 2:
            #pokemon
            return pokemon_settings
        case 3:
            #flesh and blood
            return fleshandblood_settings

# Returns whether 2 SKU's match
# 2 = Complete Match
# 1 = Partial Match 
# 0 = Don't Match
def CompareSKU(binder_sku : list, collection_sku : list, game_id : int) -> int:
    match game_id:
        case 0:
            #mtg
            return mtg.CompareSKU(binder_sku, collection_sku)
        case 1:
            #yugioh
            return yugioh.CompareSKU(binder_sku, collection_sku)
        case 2:
            #pokemon
            return pokemon.CompareSKU(binder_sku, collection_sku)
        case 3:
            #flesh and blood
            return fleshandblood.CompareSKU(binder_sku, collection_sku)
        
def GenerateSKU(collection_line : dict, game_id : int) -> dict:
    match game_id:
        case 0:
            #mtg
            return mtg.BuildSKU(collection_line)
        case 1:
            #yugioh
            return yugioh.BuildSKU(collection_line)
        case 2:
            #pokemon
            return pokemon.BuildSKU(collection_line)
        case 3:
            #flesh and blood
            return fleshandblood.BuildSKU(collection_line)
        
def GetQuantity(collection_line : dict, game_id : int) -> int:
    match game_id:
        case 0:
            #mtg
            return mtg.GetQuantity(collection_line)
        case 1:
            #yugioh
            return yugioh.GetQuantity(collection_line)
        case 2:
            #pokemon
            return pokemon.GetQuantity(collection_line)
        case 3:
            #flesh and blood
            return fleshandblood.GetQuantity(collection_line)
         
def GetReadableLine(collection : dict, game_id : int) -> str:
    match game_id:
        case 0:
            #mtg
            return mtg.GetReadableLine(collection)
        case 1:
            #yugioh
            return yugioh.GetReadableLine(collection)
        case 2:
            #pokemon
            return pokemon.GetReadableLine(collection)
        case 3:
            #flesh and blood
            return fleshandblood.GetReadableLine(collection)

