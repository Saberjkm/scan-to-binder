# This is the conversion from the Dragon Shield collection export to Binder form
condition_conversion = {
    "Mint" : "1",
    "NearMint" : "1",
    "Excellent" : "1",
    "Good" : "2",
    "LightPlayed" : "2",
    "Played" : "3",
    "Poor" : "4"
}

language_conversion = {
    "English" : "EN"
}

printing_conversion = {
    "Foil" : "FO",
    "Normal" :"NF"
}

quantity_column_name = "Quantity"

# SKU - these are split upon a '-' character to represent a list of values

# The names for the parts of an SKU
# Used to act as keys for the things that need the identification of which sku part it belongs to
sku_format_labels = list(["Set Code", "Card Number", "Language", "Foiling", "Condition"])

# These are the column names from the collection file that correspond to a required part of the SKU
# These columns are present within the collection file but not the binder one
# Instead the index of each label matches to what the binder sku would be
sku_collection_labels = list(["Set Code", "Card Number", "Language", "Printing", "Condition"])

def SetCodeConversion(set_code : str) -> str:
    return set_code

def CardNumberConversion(card_number : str):
    return card_number

def LanguageConversion(language):
    return language_conversion[language]

def PrintingConversion(printing):
    return printing_conversion[printing]

def ConditionConversion(condition):
    return condition_conversion[condition]

conversion_functions = list([SetCodeConversion, CardNumberConversion, LanguageConversion, PrintingConversion, ConditionConversion])

# This matches two Strings together and returns an int
def PartialComparison(primary_string : str, secondary_string : str) -> int:
    if (len(primary_string) == 0 or len(secondary_string) == 0):
        return 0
    if primary_string.casefold() == secondary_string.casefold():
        return 2
    if secondary_string.casefold() in primary_string.casefold():
        return 1
    if primary_string.casefold() in secondary_string.casefold():
        return 1
    return 0

# SKU Matching data

# These functions match the collection sku to various data within the binder sku

def MatchLanguage5(binder_sku : list, collection_sku : list) -> bool:
    return binder_sku[2] == collection_sku[2]

def MatchPrinting5(binder_sku : list, collection_sku : list) -> bool:
    return binder_sku[3] == collection_sku[3]

def MatchCondition5(binder_sku : list, collection_sku : list) -> bool:
    return binder_sku[4] == collection_sku[4]

# These are the absolute match functions needed for a binder input sku of a length that equals the index into this list
# The normalise function should normalise an input sku to match one of the lines that is populated
# These match absolutely so if any of the given functions return false then the function should return no match overall
absolute_match_functions = [
    [], # Length 0
    [], # Length 1
    [], # Length 2
    [], # Length 3
    [], # Length 4
    [MatchLanguage5, MatchPrinting5, MatchCondition5] # Length 5
]

def MatchSetCode5(binder_sku : list, collection_sku : list) -> int:
    return PartialComparison(binder_sku[0], collection_sku[0])

def MatchCardNumber5(binder_sku : list, collection_sku : list) -> int:
    result = PartialComparison(binder_sku[1], collection_sku[1])
    if result == 1:
        # Check for CN//CN format
        new_string = ''.join([collection_sku[1], "//", collection_sku[1]])
        if PartialComparison(binder_sku[1], new_string) == 2:
            return 2
    return result

# These are the partial match functions needed for a binder input sku of a length that equals the index into this list
# The normalise function should normalise an input sku to match one of the lines that is populated
# These match partially so a sum over the function returns gives the needed return match output
partial_match_functions = [
    [], # Length 0
    [], # Length 1
    [], # Length 2
    [], # Length 3
    [], # Length 4
    [MatchSetCode5, MatchCardNumber5] # Length 5
]

# This is what the minimun sum should be to return as a partial match
partial_match_lower_bound = 3

# Returns whether two SKU's match (specific to mtg)
# 2 = Complete Match
# 1 = Partial Match
# 0 = No Match
def CompareSKU(binder_sku : list, collection_sku : list) -> int:
    # Check the absolute matches
    for match_function in absolute_match_functions[len(binder_sku)]:
        if not match_function(binder_sku, collection_sku):
            return 0
    
    # Check the partial matches
    total = 0
    max_total_value = 0
    for match_function in partial_match_functions[len(binder_sku)]:
        max_total_value += 2
        total += match_function(binder_sku, collection_sku)
    
    if total == max_total_value:
        return 2
    if total >= partial_match_lower_bound:
        return 1
    return 0

#This builds as SKU for product look up from a given collection row
def BuildSKU(collection_line : dict) -> list:
    return_sku = list()
    
    # This maps the SKU parts to the columns within the collection line
    column_map = dict(zip(sku_format_labels, sku_collection_labels))
    
    # These get the data for an sku from the collection line
    generators = dict(zip(sku_format_labels, conversion_functions))
    
    # Go through each part of the sku
    for sku_column_name in sku_format_labels:
        return_sku.append(generators[sku_column_name](collection_line[column_map[sku_column_name]]))
    return return_sku

# These are the cull functions that run on the list of matches after the collection has been matched against the binder file
# It further does checks to narrow down the resulting matches list
# Returns true if the binder match can be culled (its not close enough to be considered a match based on other sources)

def CullMatchName(collection_item : dict, binder_match : dict) -> bool:
    return PartialComparison(binder_match["Product Title"], collection_item["raw_line"]["Card Name"]) == 0

cull_functions = [CullMatchName]

def CullMatches(collection : list):
    for collection_item in collection:
        if collection_item["perfect_match"]:
            continue
        new_matches = list()
        for binder_match in collection_item["matches"]:
            needs_cull = False
            for cull_function in cull_functions:
                if cull_function(collection_item, binder_match):
                    needs_cull = True
            if not needs_cull:
                new_matches.append(binder_match)
        collection_item["matches"] = new_matches
            
def GetQuantity(collection_line : dict) -> int:
    if quantity_column_name in collection_line:
        return collection_line[quantity_column_name]
    return 0

def GetSKUString(sku_line : list) -> str:
    return '-'.join(sku_line)

def GetSKULabels() -> dict:
    return sku_collection_labels

def GetReadableLine(collection_item : dict) -> str:
    return_string = str()
    sku_string = GetSKUString(collection_item["processed_sku"])
    if "raw_line" in collection_item:
        if "Set Code" in collection_item["raw_line"]:
            return_string += "["
            return_string += collection_item["raw_line"]["Set Code"]
            return_string += "]"
        return_string += " "
        if "Card Name" in collection_item["raw_line"]:
            return_string += collection_item["raw_line"]["Card Name"]
        return_string += " "
        if "Card Number" in collection_item["raw_line"]:
            return_string += "("
            return_string += collection_item["raw_line"]["Card Number"]
            return_string += ")"
        return_string += " "
        if "quantity" in collection_item:
            return_string += "x"
            return_string += collection_item["quantity"]
        return_string += " "
        return_string += "["
        return_string += sku_string
        return_string += "]"
    return return_string
    
def ValidateCollection(collection_sample : dict) -> bool:
    valid_collection = True
    for label in sku_collection_labels:
        if label not in collection_sample:
            valid_collection = False
    return valid_collection

# This normalises a binder sku to match the collection sku comparison functions
def NormaliseSKU(sku : str) -> list:
    processed_sku = sku.split("-")
    while len(processed_sku) != 5:
        # Missing set name so give it an empty one in the right place
        if len(processed_sku) < 5:
            processed_sku.insert(0, "")
        # Too many columns so fold excess into the cardnumber
        if len(processed_sku) > 5:
            processed_sku[1] += processed_sku[2]
            del processed_sku[2]
    return processed_sku
    