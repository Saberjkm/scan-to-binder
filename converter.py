import csv
import re
import copy

import settings.dragonshield.dragonshield as dragonshield
import settings.binder as binder
from tkinter import messagebox

# Lists the supported games
game_list = ["MTG", "Yu-Gi-Oh", "Pokemon", "Flesh and Blood"]

# Lists all the supported collection managers
collection_list = ["Dragonshield"]
collection_input_settings_list = list([dragonshield.GetCollectionInputSettings])

#Lists all the supported collection specific functions
get_quantity = [dragonshield.GetQuantity]
process_sku = [dragonshield.GenerateSKU]
normalise_sku = [dragonshield.NormaliseSKU]
compare_sku = [dragonshield.CompareSKU]
get_encoding = [dragonshield.GetEncoding]
cull_matches = [dragonshield.CullMatches]
evaluate_perfect_matches = [dragonshield.EvaluatePerfectMatches]

# List of Related Functions

# CSV sometimes have a leading line with this in it so stored for checking
collection_delimiter_regex = str("sep=.")

sku_column_name = "SKU"

# The keys of a processed collection line dictionary
collection_item_key_names = list(("id", "raw_line", "processed_sku", "quantity"))

def IDGenerator(item_id : int, collection_line : dict, game_id : int, collection_id : int) -> int:
    return item_id

def RawLineGenerator(item_id : int, collection_line : dict, game_id : int, collection_id : int) -> dict:
    return collection_line

def ProcessedSKUGenerator(item_id : int, collection_line : dict, game_id : int, collection_id : int) :
    return process_sku[collection_id](collection_line, game_id)

def QuantityGenerator(item_id : int, collection_line : dict, game_id : int, collection_id : int) :
    return get_quantity[collection_id](collection_line, game_id)

collection_item_key_generators = list([IDGenerator, RawLineGenerator, ProcessedSKUGenerator, QuantityGenerator])

# Defines the stuff needed for general use related to a particular search
binder_generation_settings = list(("Game Type"))

def CheckCSVFormat(filename : str, encoding : str) -> tuple:
    # Check for a seperator line before the header and set the proper delimiters
    # Generally only for some excel formatted csv's which some scanners use
    csv_delimiter_header_present = False
    csv_delimiter = ','
    try:
        with open(filename, 'r', encoding=encoding) as f:
            first_line = f.readline();
            match = re.search(collection_delimiter_regex, first_line)
            if match != None :
                csv_delimiter_header_present = True
                csv_delimiter = match.group()[len(collection_delimiter_regex) - 1]
    except:
        messagebox.showerror("Bad File", "Tried loading a file that was not in the correct format. Try selecting the right game.")
    return tuple([csv_delimiter_header_present, csv_delimiter])
    
def LoadCollectionInput(filename : str, collection : list, game_id : int, collection_id : int) -> bool:
    result = CheckCSVFormat(filename, get_encoding[collection_id](game_id))
    try:
        with open(filename, 'r', newline='', encoding=get_encoding[collection_id](game_id)) as collection_csvfile:
            if result[0] == True:
                collection_csvfile.readline()

            collection.clear()
            collection_input_settings = collection_input_settings_list[collection_id](game_id)
            generators = dict(zip(collection_item_key_names, collection_item_key_generators))

            collection_reader = csv.DictReader(collection_csvfile)
            for i, collection_line in enumerate(collection_reader):
                    if not collection_input_settings["validator"](collection_line):
                        messagebox.showerror("Invalid Collection File", "The collection file isn't in a valid format.")
                        return False
                    collection_item = dict()
                    for collection_item_key in collection_item_key_names:
                        collection_item[collection_item_key] = generators[collection_item_key](i, collection_line, game_id, collection_id)
                    collection.append(collection_item)
            return True
    except:
        return False
                    
def GetReadableLine(collection_index : int, game_index : int, collection_line : dict) -> str:
    match collection_index:
        case 0:
            return dragonshield.GetReadableLine(collection_line, game_index)
    return ""
            
def MatchBinderLine(binder_line : dict, collection_item : dict, game_id : int, collection_id : int):
    if collection_item["perfect_match"]:
        return
    if not binder_line["Game Type"] == binder.game_codes[game_id]:
        return
    normalised_binder_sku = normalise_sku[collection_id](binder_line[sku_column_name], game_id)
    collection_sku = collection_item["processed_sku"]
    match compare_sku[collection_id](normalised_binder_sku, collection_sku, game_id):
        case 0:
            # Binder Line doesn't match
            return
        case 1:
            # Binder Line and Collection Item partially match
            collection_item["matches"].append(binder_line)
        case 2:
            # Binder Line and Collection Item match
            collection_item["perfect_match"] = True
            collection_item["matches"] = [binder_line]
    
def GenerateBinderInputLine(collection_item : dict) -> dict:
    collection_item["matches"][0][binder.quantity_change_column_name] = collection_item["quantity"]
    return collection_item["matches"][0]

def ExportFile(filename : str, collection : dict):
    with open(filename, 'w', newline='', encoding="utf8") as export_csvfile:
        export_writer = csv.DictWriter(export_csvfile, fieldnames=binder.import_field_names)
        export_writer.writeheader()
        for collection_item in collection:
            if "bad_match" not in collection_item:
                export_writer.writerow(GenerateBinderInputLine(collection_item))

# Open the collection and sort them into an SKU dict
def GenerateBinderInput(filename : str, collection : list, game_id : int, collection_id : int) -> bool:
    try:
        with open(filename, 'r', newline='', encoding="utf8") as binder_csv:
            binder_reader = csv.DictReader(binder_csv)
            # Set up fields needed for the matching
            for collection_item in collection:
                collection_item["perfect_match"] = False
                collection_item["matches"] = list()
            for binder_line in binder_reader:
                for collection_item in collection:
                    MatchBinderLine(binder_line, collection_item, game_id, collection_id)
            cull_matches[collection_id](collection, game_id)
            evaluate_perfect_matches[collection_id](collection)
        return True
    except:
        messagebox.showerror("Binder File Error", "Error reading the binder file.")
        return False
        
    